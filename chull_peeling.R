#This script applies Gaussian mixture models from the mclust package to real data
#from files NGC2808.DD	NGC5904.DD	NGC6205.DD	NGC6441.DD

library("mclust")
library("dbscan")
library("viridis")
library("grDevices")
set.seed(3489)

read_cluster <- function(cluster_name)
{
    n <- read.table(cluster_name, header = FALSE, na.strings="99.9999")
    n <- n[!is.na(n$V1+n$V3),c(1,3)]
    names(n) <- c("C1", "C2")
    return(n)
}

chull_peeling <- function(x, k)
{
    if(k == 0) return(x)
    return(chull_peeling(x[!(1:nrow(x) %in% chull(x[,1], x[,2])),], k-1))
}

x <- read_cluster("NGC2808.DD")
x_peel <- chull_peeling(x, 10)
nrow(x)
nrow(x_peel)
plot(x$C1, x$C2, col = "#A0A0A0")
points(x_peel$C1, x_peel$C2, pch = 16)
q()

getmode <- function(v) {
   uniqv <- unique(v)
   uniqv[which.max(tabulate(match(v, uniqv)))]
}

outlier_removal <- function(x, eps = 0.02, N = 4, rows = FALSE)
{
    clustering <- dbscan(x, eps, N)$cluster
    if(rows)
    {
    	return(clustering != getmode(clustering))
    } else {
	    x <- x[clustering == getmode(clustering),]
	    return(x)
    }
}

fit_cluster <- function(cluster_name, remove_outliers = FALSE, modelName = "EEE")
{
    #Running mclust-----------------------
    x <- read_cluster(cluster_name)
    if(remove_outliers) x <- outlier_removal(x)
    init = NULL #list(noise = outlier_removal(x, 0.02, 10, TRUE))
    result <- densityMclust(x, modelNames=modelName, G = 1:8, initialization = init)
    if(nrow(x) != length(result$classification)) stop(result$classification)
    return(list(result=result, x=x))
    #-------------------------------------
}

plot_cluster <- function(cluster_name, remove_outliers = FALSE, modelName = "EEE", newPlot = TRUE)
{
	if(newPlot) pdf(paste(cluster_name, ".pdf", sep = ""))
	fitted <- fit_cluster(cluster_name, remove_outliers)
	result <- fitted$result
	x <- fitted$x
    cbfriendly <- viridis(max(result$classification)) 
    cols <- cbfriendly[result$classification]
    pchs <- (1:max(result$classification))[result$classification]
    plot(x$C1, x$C2, col = cols, pch = pchs, cex = 0.5, xlab = expression(Delta*"F275W,F814W"), ylab = expression(Delta*"C F275W,F336W,F438W"), main = modelName)
    #plot(result, what = "density", type = "level", data = x, points.cex = 0.5)
    plot(result, what = "BIC")
}

plot_cluster("NGC2808.DD", remove_outliers = TRUE)
sapply(mclust.options("emModelNames"), function(m) plot_cluster("NGC2808.DD", remove_outliers = TRUE, modelName = m, newPlot = FALSE)) 
plot_cluster("NGC5904.DD", remove_outliers = TRUE)
plot_cluster("NGC6205.DD", remove_outliers = TRUE)
plot_cluster("NGC6441.DD", remove_outliers = TRUE)



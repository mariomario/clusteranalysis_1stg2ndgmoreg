#!/bin/bash
for nome in $(ls *.R)
do 
    cat $nome | sed s/'F438W,F814W'/'F275W,F814W'/g | sed s/'F336W,F343N,F438W'/'F275W,F336W,F438W'/g > tmp
    mv tmp $nome
done

library("dbscan")
library("viridis")
library("cluster")
set.seed(3489)

read_cluster <- function(cluster_name)
{
    n <- read.table(cluster_name, header = FALSE, na.strings="99.9999")
    n <- n[!is.na(n$V1+n$V3),c(1,3)]
    names(n) <- c("C1", "C2")
    return(n) 
}

getmode <- function(v) {
   uniqv <- unique(v)
   uniqv[which.max(tabulate(match(v, uniqv)))]
}

outlier_removal <- function(x, eps = 0.02, N = 4, rows = FALSE)
{
    clustering <- dbscan(x, eps, N)$cluster
    if(rows)
    {
    	return(clustering != getmode(clustering))
    } else {
	    x <- x[clustering == getmode(clustering),]
	    return(x)
    }
}

sortwell <- function(v, cluster)
{
    itis <- (1:max(cluster))
    butitshouldbe <- rank(tapply(v, cluster, median))
    return(butitshouldbe[cluster])
}

agglo <- function(x, method, k, par.method = 0)
{
    if(method != "gaverage") cluster <- cutree(agnes(x, method=method), k=k)
    if(method == "gaverage") cluster <- cutree(agnes(x, method=method, par.method = par.method), k=k)
    cluster <- sortwell(x$C2, cluster) #sorting so that the groups are in order of increasing median
    return(list(cluster = cluster))
}

plot_dendrogram <- function(x, method, hcut)
{
    agnesObject <- agnes(x, method=method)
    stump <- cut(as.dendrogram(as.hclust(agnesObject)), h = hcut)$upper
    plot(stump, edge.root=TRUE, leaflab = "none", frame.plot = FALSE, axes = FALSE)
    axis(2, at = (0:5)/10.0)
    #box()
    return(agnesObject)
}

x <- read_cluster("NGC2808.DD")
setEPS()
postscript("human_00.eps")
plot(x, pch = ".", xlab = expression(Delta*"F275W,F814W"), ylab = expression(Delta*"C F275W,F336W,F438W"))

l0 <- c(-0.50, 0)
lines(l0, 0.84 + 1.1*l0, lwd = 2, col = "#888888")
lines(c(-2.0, l0[1]), rep(0.84 + 1.1*l0[1], 2), lwd = 2, col = "#888888")

l1 <- c(-0.35, 1)
m <- 0.5
lines(l1, 0.4 + m*l1, lwd = 2, col = "#888888")
mm <- 0.75
qq <- 0.4 + m*l1[1] - mm*l1[1]
lines(l1, qq + mm*l1, lwd = 2, col = "#A0A0A0", lty = 2)
lines(c(-2.0, l1[1]), rep(0.4 + m*l1[1], 2), lwd = 2, col = "#888888")
#lines(c(-2.0, l1[1]), rep(0.4 + m*l1[1], 2) + 0.003, lwd = 2, col = "#A0A0A0", lty = 2)

inbetween <- (x$C2 < qq + mm*x$C1) & (x$C2 > 0.4 + m*x$C1)
points(x[inbetween,], col = "seagreen", cex = 3, pch = ".") ##F7EF99

l2 <- c(-0.44, 2)
lines(l2, 0.08 - 0.33*l2, lwd = 2, col = "#888888")

l3 <- c(-0.2, 0)
lines(l3, 0.08 + 2.0*l3, lwd = 2, col = "#888888")

nrow(x[inbetween,])
nrow(x[inbetween,])/nrow(x)



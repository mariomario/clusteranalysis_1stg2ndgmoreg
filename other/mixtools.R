library("mixtools")

set.seed(3489)

delta <- 0.3
Nrealclu <- 2
N1 <- 350
N0 <- 0
N2 <- 550
mux1 <- 0.5
mux2 <- 0.5 + delta
muy1 <- 0.5
muy2 <- 0.5
sigma <- 0.1
a <- c(mux1+sigma*rnorm(N1), runif(N0), mux2+sigma*rnorm(N2)) 
b <- c(muy1+sigma*rnorm(N1), runif(N0), muy2+sigma*rnorm(N2))
realclu <- c(rep(1, N1), rep(0,N0), rep(2,N2))
x <- data.frame(a,b)

result <- mvnormalmixEM(x, k=2)
result

col <- rgb(result$posterior[,1], 0, result$posterior[,2]) #red or blue depending on group

plot(x$a, x$b, col = col, pch = 16)



